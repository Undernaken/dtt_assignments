"""
this code try to solve a given assignment: flatten a nested (any depth) array

coded by M.Boschi on 05 Feb 2019
based on python 3.7 interpreter
"""
from collections.abc import Iterable
from typing import Generator


def flatten(items) -> Generator:
    '''
    :param items: a list or tuple of elements with a variable nesting depth
    :return a generator iterator over the current element in the unfolded list:
    '''
    for i in items:
        if isinstance(i, Iterable):
            for t in flatten(i):
                yield t
        else:
            yield i


def flatten_acc(items, acc):
    '''
    :param acc: the unfloding accumulator
    :param items: a list or tuple of elements with a variable nesting depth
    :return a generator iterator over the current element in the unfolded list:
    '''
    if not isinstance(acc, list):
        raise ValueError("acc paramenter must be an instance of a list")
    for i in items:
        if isinstance(i, Iterable):
            flatten_acc(i, acc)
            continue
        acc.append(i)


