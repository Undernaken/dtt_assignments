import unittest

from flatten import flatten, flatten_acc


class TestFlatten(unittest.TestCase):

    def test_input_list(self):
        narray2 = [0, 1, 2, 3, 4]
        res = list(flatten(narray2))
        self.assertEqual(len(res), 5)
        self.assertEqual(res, [0, 1, 2, 3, 4])

    def test_input_list1(self):
        narray2 = [[[[0, [1, [[[2]]]]]]], 3, 4, [6, [7, [8]]]]
        res = list(flatten(narray2))
        self.assertEqual(len(res), 8)
        self.assertEqual(res, [0, 1, 2, 3, 4, 6, 7, 8])

    def test_input_list2(self):
        narray = [1, [2, [3, [4, ]]], 5, 6, [7, [8, [10, [[[[11]]]]], 9, 10, [11, [12, ]]]]]
        res = list(flatten(narray))
        self.assertEqual(len(res), 14)
        self.assertEqual(res, [1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 9, 10, 11, 12])

    def test_input_empty_list(self):
        narray = []
        res = list(flatten(narray))
        self.assertEqual(len(res), 0)
        self.assertEqual(res, [])

    def test_acc_input_list(self):
        narray2 = [0, 1, 2, 3, 4]
        res = []
        flatten_acc(narray2, res)
        self.assertEqual(len(res), 5)
        self.assertEqual(res, [0, 1, 2, 3, 4])

    def test_acc_input_list1(self):
        narray2 = [[[[0, [1, [[[2]]]]]]], 3, 4, [6, [7, [8]]]]
        res = []
        flatten_acc(narray2, res)
        self.assertEqual(len(res), 8)
        self.assertEqual(res, [0, 1, 2, 3, 4, 6, 7, 8])

    def test_acc_input_list2(self):
        narray = [1, [2, [3, [4, ]]], 5, 6, [7, [8, [10, [[[[11]]]]], 9, 10, [11, [12, ]]]]]
        res = []
        flatten_acc(narray, res)
        self.assertEqual(len(res), 14)
        self.assertEqual(res, [1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 9, 10, 11, 12])

    def test_acc_input_empty_list(self):
        narray = []
        res = []
        flatten_acc(narray, res)
        self.assertEqual(len(res), 0)
        self.assertEqual(res, [])
