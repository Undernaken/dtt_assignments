using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using TcpServerConsole.Client;
using TcpServerConsole.Server;
using TcpServerConsole.Server.Connection;
using TcpServerConsole.Server.Strategy;

namespace TcpServerConsole.Tests
{
    /// <summary>
    /// TestMirrorStrategy helps to test the tcp server about connections and data transferred
    /// here the strategy is "mirror" so for a message sent by a client, the same message will be delivered to each connected client
    /// </summary>
    [TestFixture]
    public class TestMirrorStrategy
    {
        private ServerManager _serverManager;
        private const int ConnectionsToTest = 100;
        private const string toSend = "hello I am a client";
        private readonly List<TcpClient> _clients = new List<TcpClient>();

        [SetUp]
        public void SetUp()
        {
            _serverManager = new ServerManager("127.0.0.1", 10000, new MirrorStrategy(), new EndLineMessageParser());
            _serverManager.StartAsync();
        }

        [Test]
        public void Test()
        {
            for (var i = 0; i < ConnectionsToTest; i++)
            {
                var client = new TcpClient("127.0.0.1", 10000);
                client.Connect();
                _clients.Add(client);
            }

            // client in '0' position send a message
            var sent = _clients[0].Send(Encoding.UTF8.GetBytes(toSend + Constants.NL));
            Assert.AreEqual(sent, true);
            var payload = _clients[2].Receive();
            // client in '2' position should receive the same message sent by the client '0'
            var received = Encoding.UTF8.GetString(payload);
            Assert.AreEqual(received, toSend);
        }

        [TearDown]
        public void TearDown()
        {
            _serverManager.Dispose();
        }

        /// <summary>
        /// this strategy send the same message received to all connected clients
        /// </summary>
        private class MirrorStrategy : IResponseStrategy
        {
            public void OnMessage(ConnectionHandler sender, IList<ConnectionHandler> allConnections, byte[] message)
            {
                foreach (var connection in allConnections)
                    connection.Send(message);
            }
        }
    }
}