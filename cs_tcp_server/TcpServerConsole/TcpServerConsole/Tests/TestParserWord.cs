using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using TcpServerConsole.Client;
using TcpServerConsole.Server;
using TcpServerConsole.Server.Connection;
using TcpServerConsole.Server.Strategy;

namespace TcpServerConsole.Tests
{
    /// <summary>
    /// TestParseWord helps to test the tcp server about message parsing
    /// here the strategy is "mirror" so for a message sent by a client, the same message will be delivered to each connected client
    /// and the message parser is to replay the same message when the message contains the a specific trigger word <see cref="TriggerWord"/>
    /// otherwise the message will be 'ko'
    /// </summary>
    [TestFixture]
    public class TestParseWord
    {
        private ServerManager _serverManager;
        private const int ConnectionsToTest = 100;
        private const string ToSendOk = "Hi this is matteo";
        private const string ToSendKo = "Hi this is luca";
        public const string TriggerWord = "matteo";
        private readonly List<TcpClient> _clients = new List<TcpClient>();

        [SetUp]
        public void SetUp()
        {
            _serverManager = new ServerManager("127.0.0.1", 10000, new MirrorStrategy(), new MessageWordParser());
            _serverManager.StartAsync();
        }

        [Test]
        public void Test()
        {
            for (var i = 0; i < ConnectionsToTest; i++)
            {
                var client = new TcpClient("127.0.0.1", 10000);
                client.Connect();
                _clients.Add(client);
            }

            // client in '0' position send a message
            var sent = _clients[0].Send(Encoding.UTF8.GetBytes(ToSendOk));
            Assert.AreEqual(sent, true);
            var payload = _clients[2].Receive();
            // client in '2' position should receive the same message sent by the client '0'
            var received = Encoding.UTF8.GetString(payload);
            Assert.AreEqual(received, ToSendOk);
            // client in '0' position send a message
            sent = _clients[10].Send(Encoding.UTF8.GetBytes(ToSendKo));
            Assert.AreEqual(sent, true);
            payload = _clients[2].Receive();
            // client in '2' position should receive the same message sent by the client '0'
            received = Encoding.UTF8.GetString(payload);
            Assert.AreNotEqual(received, ToSendOk);
        }

        [TearDown]
        public void TearDown()
        {
            _serverManager.Dispose();
        }

        /// <summary>
        /// this strategy send the same message received to all connected clients
        /// </summary>
        private class MirrorStrategy : IResponseStrategy
        {
            public void OnMessage(ConnectionHandler sender, IList<ConnectionHandler> allConnections, byte[] message)
            {
                foreach (var connection in allConnections)
                    connection.Send(message);
            }
        }
    }

    public class MessageWordParser : IMessageParser
    {
        private string _current = "";
        private byte[] _payload;
        public byte[] Payload => _payload;

        public bool Parse(byte[] payload)
        {
            _current += Encoding.UTF8.GetString(payload);
            if (_current.Contains(TestParseWord.TriggerWord))
            {
                var value = _current.Substring(0,
                    _current.IndexOf(TestParseWord.TriggerWord, StringComparison.Ordinal) + TestParseWord.TriggerWord.Length);
                _payload = Encoding.UTF8.GetBytes(value);
                _current = "";
                return true;
            }
            _payload = Encoding.UTF8.GetBytes("ko");
            return true;
        }
    }
}