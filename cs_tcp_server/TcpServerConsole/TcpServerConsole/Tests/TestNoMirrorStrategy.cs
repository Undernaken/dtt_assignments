using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using TcpServerConsole.Client;
using TcpServerConsole.Server;
using TcpServerConsole.Server.Connection;
using TcpServerConsole.Server.Strategy;

namespace TcpServerConsole.Tests
{
    /// <summary>
    /// TestNoMirrorStrategy helps to test the tcp server about connections and data transferred
    /// here the strategy is "no mirror" so for a message sent by a client, the same message will be delivered to each connected client excpet the sender.
    /// the sender will be receive a different message
    /// </summary>
    [TestFixture] 
    public class TestNoMirrorStrategy
    {
        private ServerManager _serverManager;
        private const int ConnectionsToTest = 100;
        private const string toSend = "hello I am a client";
        private const string toSendToSender = "you are the sender";
        private readonly List<TcpClient> _clients = new List<TcpClient>();

        [SetUp]
        public void SetUp()
        {
            _serverManager = new ServerManager("127.0.0.1", 10000, new MirrorStrategy(), new EndLineMessageParser());
            _serverManager.StartAsync();
        }

        [Test]
        public void Test()
        {
            for (var i = 0; i < ConnectionsToTest; i++)
            {
                var client = new TcpClient("127.0.0.1", 10000);
                client.Connect();
                _clients.Add(client);
            }

            // client in '0' position send a message
            var sent = _clients[0].Send(Encoding.UTF8.GetBytes(toSend + Constants.NL));
            Assert.AreEqual(sent, true);
            var payload = _clients[2].Receive();
            // client in '2' position should receive the same message sent by the client '0'
            var received = Encoding.UTF8.GetString(payload);
            Assert.AreEqual(received, toSend);

            //client in '0' (the sender) should receive a specific message as the strategy wants
            payload = _clients[0].Receive();
            received = Encoding.UTF8.GetString(payload);
            Assert.AreEqual(received, toSendToSender );
        }

        [TearDown]
        public void TearDown()
        {
            _serverManager.Dispose();
        }

        
        /// <summary>
        /// this strategy send the same message received to all connected clients except the sender.
        /// The sender will receive a specific message <see cref="TestNoMirrorStrategy.toSendToSender"/>
        /// </summary>
        private class MirrorStrategy : IResponseStrategy
        {
            public void OnMessage(ConnectionHandler sender, IList<ConnectionHandler> allConnections, byte[] message)
            {
                foreach (var connection in allConnections)
                {
                    if (connection.Equals(sender))
                    {
                        connection.Send(Encoding.UTF8.GetBytes(toSendToSender));
                        continue;
                    }

                    connection.Send(message);
                }
            }
        }
    }
}