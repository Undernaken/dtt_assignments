using System;
using System.Collections.Generic;
using NUnit.Framework;
using TcpServerConsole.Client;
using TcpServerConsole.Server;

namespace TcpServerConsole.Tests
{
    /// <summary>
    /// this test is about connections. If some of tried connections don't work an exception will be raised and it will invalidate the test
    /// </summary>
    [TestFixture]
    public class TestConnections
    {
        private ServerManager _serverManager;
        private const int ConnectionsToTest = 100;
        private readonly List<TcpClient> _clients = new List<TcpClient>();

        [SetUp]
        public void SetUp()
        {
            _serverManager = new ServerManager("127.0.0.1", 10000);
            _serverManager.StartAsync();
        }

        [Test]
        public void Test()
        {
            try
            {
                for (var i = 0; i < ConnectionsToTest; i++)
                {
                    var client = new TcpClient("127.0.0.1", 10000);
                    client.Connect();
                    _clients.Add(client);
                }
            }
            catch (Exception e)
            {
                Assert.Fail("Expected no exception, but got: " + e.Message);
            }
        }

        [TearDown]
        public void TearDown()
        {
            _serverManager.Dispose();
        }
    }
}