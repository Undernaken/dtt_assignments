import socket

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 10000        # The port used by the server

from time import sleep

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    b = s.recv(1024)
    print("received '%s'" % str(b,'utf-8'))