﻿using System;
using TcpServerConsole.Server;
using TcpServerConsole.Server.Connection;
using TcpServerConsole.Server.Strategy;

namespace TcpServerConsole
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var address = Constants.DefaultAddress;
            var port = Constants.DefaultPort;
            // try to parse input args + some little checks to sanitize them
            if (args.Length == 2)
            {
                address = args[0].Trim();
                if (address.Length == 0)
                {
                    Console.WriteLine("Input error: address cannot be empty");
                    return;
                }
                if (!int.TryParse(args[1], out port))
                {
                    Console.WriteLine("Input error: port must be an integer");
                    return;
                } 
            }
            Console.WriteLine($"starting tcp server on interface {address}:{port}");
            // to start a manger you have to provide an address and a port to bind it
            // you can define:
            // your own strategy when a message is received
            // your own message parser
            using (var manager = new ServerManager(address, port, new BroadcastStrategy(), new EndLineMessageParser()))
            {
                manager.Start();
            }
        }
    }
}