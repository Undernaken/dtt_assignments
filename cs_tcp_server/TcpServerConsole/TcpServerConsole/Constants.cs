namespace TcpServerConsole
{
    public static class Constants
    {
        public static string DefaultAddress => "127.0.0.1";
        public static int DefaultPort => 10000;
        public static string NL => "\r\n";
    }
}