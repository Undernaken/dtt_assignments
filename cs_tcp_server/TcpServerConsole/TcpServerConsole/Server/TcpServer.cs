using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace TcpServerConsole.Server
{
    public class TcpServer : IServer
    {
        private TcpListener _server;
        private readonly int _port;
        private readonly string _address;

        private Task _listenTask;
        private bool _exit;

        public TcpServer(string address, int port)
        {
            _port = port;
            _address = address;
        }

        /// <summary>
        /// Listen in a non blocking way
        /// </summary>
        public override void ListenAsync()
        {
            _listenTask = new Task(Listen);
            _listenTask.Start();
        }

        /// <summary>
        /// Listen in a blocking way
        /// </summary>
        public override void Listen()
        {
            try
            {
                IPAddress localAddr = IPAddress.Parse(_address);
                _server = new TcpListener(localAddr, _port);
                _server.Start();
                while (!_exit)
                {
                    var tSocket = _server.AcceptSocket();
                    // when a connection is right accepted an event is fired to handle this connection
                    OnNewConnection?.Invoke(tSocket);
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                Stop();
            }
        }

        public override void Stop()
        {
            _exit = true;
            _server?.Stop();
        }

        public override event OnConnectionDelegate OnNewConnection;
    }
}