using System;
using System.Collections.Generic;
using System.Net.Sockets;
using TcpServerConsole.Server.Connection;
using TcpServerConsole.Server.Strategy;

namespace TcpServerConsole.Server
{
    /// <summary>
    /// ServerManager provides to start and run a server (tcp server in this case) on the specified address and port
    /// It can be started in async or sync mode. It should be used as a Disposable so at the end of the context
    /// all resources will be disposed
    /// It's possible to define a strategy <see cref="IResponseStrategy"/> that will be applied when a message arrives from a clients. If no strategy is specified
    /// the server doesn't take any action
    /// It's possible to define a parser <see cref="IMessageParser"/>  of the message received. If this parameter is null the message will be forwarded as it is
    /// </summary>
    public class ServerManager : IDisposable
    {
        private readonly IServer _server;
        private readonly IList<ConnectionHandler> _connectionHandlers;
        private IResponseStrategy _strategy;
        private readonly IMessageParser _messageParser;

        public int ActiveConnections => _connectionHandlers.Count;

        /// <summary>
        /// public ServerManager constructor
        /// </summary>
        /// <param name="address">the server bind address</param>
        /// <param name="port">the server bind port</param>
        /// <param name="strategy">the strategy the server will apply when a message is received</param>
        /// <param name="parser">the parser applied to a message when it will be received</param>
        public ServerManager(string address, int port, IResponseStrategy strategy = null, IMessageParser parser = null)
        {
            _server = new TcpServer(address, port);
            _server.OnNewConnection += ServerOnOnNewConnection;
            _connectionHandlers = new List<ConnectionHandler>();
            _strategy = strategy;
            _messageParser = parser;
        }

        /// <summary>
        /// start the server
        /// </summary>
        public void Start()
        {
            _connectionHandlers?.Clear();
            _server.Listen();
        }


        /// <summary>
        /// start the server in non blocking way
        /// </summary>
        public void StartAsync()
        {
            _connectionHandlers?.Clear();
            _server.ListenAsync();
        }

        #region handlers 

        /// <summary>
        /// handler called when a new connection is accepted by the server
        /// </summary>
        /// <param name="socket">the socket representing the new connection</param>
        private void ServerOnOnNewConnection(Socket socket)
        {
            var ch = new ConnectionHandler(Guid.NewGuid().ToString(), socket, parser: _messageParser);
            ch.OnNewMessage += OnNewMessage;
            ch.OnLoseConnection += OnLoseConnection;
            _connectionHandlers.Add(ch);
        }

        /// <summary>
        /// handler called when a connection is lost or closed by the client
        /// </summary>
        /// <param name="handler">the connection's handler</param>
        private void OnLoseConnection(ConnectionHandler handler)
        {
            if (!_connectionHandlers.Contains(handler)) return;
            handler.OnNewMessage -= OnNewMessage;
            handler.OnLoseConnection -= OnLoseConnection;
            _connectionHandlers.Remove(handler);
        }

        /// <summary>
        /// handler called when a new message is received
        /// </summary>
        /// <param name="sender">the connection's handler</param>
        /// <param name="message">the payload received</param>
        private void OnNewMessage(ConnectionHandler sender, byte[] message)
        {
            _strategy?.OnMessage(sender, new List<ConnectionHandler>(_connectionHandlers), message);
        }

        #endregion

        public IResponseStrategy ResponseStrategy
        {
            get => _strategy;
            set => _strategy = value;
        }

        public void Dispose()
        {
            //cleaning all handlers make a copy of the list to avoid list modification (if in async mode)
            var toClean = new List<ConnectionHandler>(_connectionHandlers);
            foreach (var ch in toClean)
                ch.Dispose();

            //stop the server
            _server.Stop();
        }
    }
}