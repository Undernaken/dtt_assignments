using System.Collections.Generic;
using TcpServerConsole.Server.Connection;

namespace TcpServerConsole.Server.Strategy
{
    /// <summary>
    /// IResponseStrategy define the shape of a strategy when a message is received. 
    /// </summary>
    public interface IResponseStrategy
    {
        /// <summary>
        /// triggered when a new message is received
        /// </summary>
        /// <param name="sender">the connection's handler where the message is received</param>
        /// <param name="allConnections">all connections handled in that moment by the server</param>
        /// <param name="message">the payload</param>
        void OnMessage(ConnectionHandler sender, IList<ConnectionHandler> allConnections, byte[] message);
    }
}