using System.Collections.Generic;
using TcpServerConsole.Server.Connection;

namespace TcpServerConsole.Server.Strategy
{
    
    
    public class BroadcastStrategy : IResponseStrategy
    {
        public void OnMessage(ConnectionHandler sender, IList<ConnectionHandler> allConnections, byte[] message)
        {
            foreach (var connection in allConnections)
            {
                if (connection.Equals(sender))
                    continue;
                connection.Send(message);
            }   
        }
    }
}