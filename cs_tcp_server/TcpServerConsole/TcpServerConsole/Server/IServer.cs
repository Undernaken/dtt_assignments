using System.Net.Sockets;

namespace TcpServerConsole.Server
{
    /// <summary>
    /// IServer represent the shape of a generic Server should be
    /// </summary>
    public abstract class IServer
    {
        public abstract event OnConnectionDelegate OnNewConnection;
        public delegate void OnConnectionDelegate(Socket newSocket);

        public abstract void Listen();
        public abstract void ListenAsync();
        public abstract void Stop();
    }
}