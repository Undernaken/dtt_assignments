namespace TcpServerConsole.Server.Connection
{
    /// <summary>
    /// IMessageParser define the shape of how a message should be parsed
    /// </summary>
    public interface IMessageParser
    {
        byte[] Payload { get; }
        /// <summary>
        /// Parse must return true when a valid payload is ready to be dispatched, false otherwise
        /// </summary>
        /// <param name="payload">the message payload received</param>
        /// <returns>true if the payload is ready to be dispatched, false otherwise</returns>
        bool Parse(byte[] payload);
    }
}