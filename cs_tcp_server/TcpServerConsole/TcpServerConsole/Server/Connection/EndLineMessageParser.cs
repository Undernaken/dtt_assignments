using System;
using System.Text;

namespace TcpServerConsole.Server.Connection
{
    public class EndLineMessageParser : IMessageParser
    {
        public byte[] Payload => _payload;

        private byte[] _payload;
        private string _current = "";


        public bool Parse(byte[] payload)
        {
            _current += Encoding.UTF8.GetString(payload);
            if (!_current.Contains(Constants.NL)) return false;
            if (_current.EndsWith(Constants.NL))
            { 
                _payload = Encoding.UTF8.GetBytes(_current.Substring(0,_current.IndexOf(Constants.NL, StringComparison.InvariantCulture)));
                _current = "";
            }
            else
            { 
                _payload = Encoding.UTF8.GetBytes(_current.Substring(0,_current.IndexOf(Constants.NL, StringComparison.InvariantCulture)));
                _current = _current.Substring(_current.IndexOf(Constants.NL, StringComparison.InvariantCulture) + Constants.NL.Length);
            }
            return true;
        }
    }
}