using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TcpServerConsole.Server.Connection
{
    /// <summary>
    /// ConnectionHandler takes care of a connection. A new task is spawned to handle the connection
    /// It provide a reading loop and an event when a message is received
    /// If a <see cref="IMessageParser"/> is define when some data incoming will be processed by this parser
    /// </summary>
    public class ConnectionHandler : IEquatable<ConnectionHandler>
    {
        private Socket _socket;
        private readonly byte[] _buffer;
        private readonly Task _task;
        private readonly string _uuid;
        private IMessageParser _parser;
        private CancellationTokenSource _cts;
        public event OnMessageDelegate OnNewMessage;


        public delegate void OnMessageDelegate(ConnectionHandler handler, byte[] message);

        public event OnCloseConnectionDelegate OnLoseConnection;

        public delegate void OnCloseConnectionDelegate(ConnectionHandler handler);

        public ConnectionHandler(string uuid, Socket socket, int dataBufferSize = 1024, IMessageParser parser = null)
        {
            _uuid = uuid;
            _socket = socket;
            _buffer = new byte[dataBufferSize];
            _parser = parser;
            _cts = new CancellationTokenSource();
            _task = new Task(Handle);
            _task.Start();
        }

        private void Handle()
        {
            try
            {
                _cts.Token.ThrowIfCancellationRequested();
                while (true)
                {
                    var received = _socket.Receive(_buffer);
                    if (received == 0 || !_socket.Connected)
                    {
                        Dispose();
                        break;
                    }

                    var data = new byte[received];
                    Array.Copy(_buffer, 0, data, 0, received);
                    if (_parser != null)
                    {
                        if (_parser.Parse(data))
                        {
                            var payload = new byte[_parser.Payload.Length];
                            Array.Copy(_parser.Payload, 0, payload, 0, _parser.Payload.Length);
                            OnNewMessage?.Invoke(this, payload);
                        }
                        continue;
                    }

                    OnNewMessage?.Invoke(this, data);
                }
            }
            catch (Exception ex)
            {
                Dispose();
            }
        }

        public bool Send(byte[] payload)
        {
            var written = 0;
            if (_socket != null && _socket.Connected)
                written = _socket.Send(payload);
            return written > 0;
        }


        public void Dispose()
        {
            _cts.Cancel();
            if (_socket.Connected)
                _socket.Disconnect(false);
            _socket.Dispose();
            OnLoseConnection?.Invoke(this);
        }

        public bool Equals(ConnectionHandler other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(_uuid, other._uuid);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ConnectionHandler) obj);
        }

        public override int GetHashCode()
        {
            return (_uuid != null ? _uuid.GetHashCode() : 0);
        }
    }
}