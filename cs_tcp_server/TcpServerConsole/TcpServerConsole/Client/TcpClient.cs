
using System;

namespace TcpServerConsole.Client
{
    public class TcpClient
    {
        private System.Net.Sockets.TcpClient _client;
        private string _serverAddress;
        private int _serverPort;
        private readonly byte[] _buffer;
        
        public TcpClient(string serverAddress, int serverPort, int dataBufferSize = 1024)
        {
            _serverAddress = serverAddress;
            _serverPort = serverPort;
            _buffer = new byte[dataBufferSize];
        }

        public void Connect()
        {
            if (_client != null && _client.Connected)
                return;
            _client = new System.Net.Sockets.TcpClient(_serverAddress, _serverPort);
        }
        

        public void Disconnect()
        {
            if (_client == null || !_client.Connected)
                return;
            _client.GetStream().Close();
            _client.Close();
        }

        public bool Send(byte[] payload)
        {
            if (_client == null || !_client.Connected)
                return false;
            try
            {
                _client.GetStream().Write(payload,0,payload.Length);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            } 
        }

        public byte[] Receive()
        {
            var received = _client.GetStream().Read(_buffer, 0, _buffer.Length);
            var data = new byte[received];
            Array.Copy(_buffer, 0, data,0, received);
            return data;
        }
    }
}